<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class UrlService
{
    public function fetchRssLinks(string $url)
    {
        $links = [];
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', $url);
            $content = $response->getContent();
            $x = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA);
            $c = $x->channel;
            $n = count($x->channel->item);
            for ($i = 1; $i < $n; $i++) {
                $h = $c->item[$i]->link;
                $links[$i] = (string) $h[0];
            }
        } catch (\Exception $e) {
            // do nothing
        }

        return $links;
    }

    public function fetchJsonLinks(string $url)
    {
        $links = [];
        $client = HttpClient::create();
        $response = $client->request('GET', $url);
        $json = $response->toArray();

        foreach ($json['articles'] as $article) {
            if (empty($article['urlToImage']) || strlen($article['urlToImage']) == 0) {
                continue;
            }
            $links[] = $article['url'];
        }

        return $links;
    }

}