<?php

namespace App\Controller;


use App\Service\UrlService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\ItemInterface;

class Home extends AbstractController
{
    private $urlService;
    private $cache;

    public function __construct(UrlService $urlService)
    {
        $this->urlService = $urlService;
        $this->cache = new FilesystemAdapter();
    }

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $ls = $this->getCachedLinks('rss_links', 'http://www.commitstrip.com/en/feed/', 'fetchRssLinks');
        $ls2 = $this->getCachedLinks('json_links', 'https://newsapi.org/v2/top-headlines?country=us&apiKey=c782db1cd730403f88a544b75dc2d7a0', 'fetchJsonLinks');
        $filteredLinks = $this->filterLinks($ls, $ls2);

        $images = $this->fetchImages($filteredLinks);

        return $this->render('default/index.html.twig', ['images' => $images]);
    }

    private function getCachedLinks(string $cacheKey, string $url, string $fetchMethod)
    {
        $links = $this->cache->get($cacheKey, function (ItemInterface $item) use ($url, $fetchMethod) {
            return $this->urlService->$fetchMethod($url);
        });

        return $links;
    }

    private function filterLinks($links1, $links2)
    {
        $filteredLinks = [];

        foreach ($links1 as $k => $v) {
            if (!$this->isDuplicate($v, $links2)) {
                $filteredLinks[$k] = $v;
            }
        }

        foreach ($links2 as $k2 => $v2) {
            if (!$this->isDuplicate($v2, $links1)) {
                $filteredLinks[$k2] = $v2;
            }
        }

        return $filteredLinks;
    }

    private function isDuplicate($link, $links)
    {
        foreach ($links as $v) {
            if ($v == $link) {
                return true;
            }
        }

        return false;
    }

    private function fetchImages($links)
    {
        $images = [];
        foreach ($links as $link) {
            try {
                $image = $this->retrieveImageFromPage($link);
                $images[] = $image;
            } catch (\Exception $e) {
                // error handling
            }
        }

        return $images;
    }

    private function retrieveImageFromPage($link)
    {
        $doc = new \DOMDocument();
        @$doc->loadHTMLFile($link);
        $xpath = new \DOMXPath($doc);
        if (strstr($link, "commitstrip.com")) {
            $xq = $xpath->query('//img[contains(@class,"size-full")]/@src');
        } else {
            $xq = $xpath->query('//img/@src');
        }
        $src = $xq[0]->value;

        return $src;
    }
}