test-dev
========

Un stagiaire à créer le code contenu dans le fichier src/Controller/Home.php

Celui permet de récupérer des urls via un flux RSS ou un appel à l’API NewsApi. 
Celles ci sont filtrées (si contient une image) et dé doublonnées. 
Enfin, il faut récupérer une image sur chacune de ces pages.

Le lead dev n'est pas très satisfait du résultat, il va falloir améliorer le code.

Pratique : 
1. Revoir complètement la conception du code (découper le code afin de pouvoir ajouter de nouveaux flux simplement) 

Questions théoriques : 
1. Que mettriez-vous en place afin d'améliorer les temps de réponses du script
 ==> J'ai optimisé le code pour améliorer les temps de réponse et j'ai utilisé des outils tels que HttpClient de Symfony 
     pour récupérer les liens RSS et JSON. J'ai également utilisé le système de cache pour réduire la charge 
     sur les ressources et accélérer les temps de réponse
2. Comment aborderiez-vous le fait de rendre scalable le script (plusieurs milliers de sources et images)
==> J'ai décomposé l'application en services plus petits et indépendants les uns des autres, 
    ce qui permet de les mettre à l'échelle individuellement. Chaque service se concentre sur des fonctionnalités
    spécifiques, comme la récupération de liens RSS et JSON. Cette approche de découpage permet une meilleure 
    gestion de l'évolutivité de l'application, car chaque service peut être ajusté et dimensionné 
    en fonction de ses besoins spécifiques,
